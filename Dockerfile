FROM eclipse-mosquitto:latest AS base
RUN apk update && apk add gettext

COPY ./mosquitto.conf /mosquitto/config/mosquitto.conf.template
COPY ./DigiCertGlobalRootG2.crt.pem /mosquitto/config/DigiCertGlobalRootG2.crt.pem
COPY ./ecc-private.key /mosquitto/config/ecc-private.key
COPY ./ecc-server.crt /mosquitto/config/ecc-server.crt
COPY ./passwords /mosquitto/config/passwords.template
RUN chmod 0700 /mosquitto/config/passwords.template
COPY ./mosquitto_start.sh /mosquitto/mosquitto_start.sh
RUN chmod u+x /mosquitto/mosquitto_start.sh
ENV LISTEN_PORT=8884
ENV IGNORE_BROKER_TLS_VALIDATION=false
ENTRYPOINT  ["/mosquitto/mosquitto_start.sh"]
