# MosquttoToAzureIOTHubBridge

mosquitto broker config to bridge local connections on port 8884 to an azure IOT hub. The bridge broker uses TLS with a TLS ECC server certificate. To connect an AD009 you have to upload the AD009_ca.pem to the device. 

start the container (e.g. using docker compose). Make sure you set these environment variables
* AZURE_DEVICE_NAME = Device name in azure iot hub
* AZURE_MQTT_HOST = your mqtt host name the bridge will publish the messages to
* AZURE_DEVICE_USER = user name to authenticate to azure iot-edge or iot hub
  * for iot hub it looks like this: AZURE_HUB_NAME.azure-devices.net/AZURE_DEVICE_NAME/?api-version=2021-04-12
* AZURE_DEVICE_PASSWORD = sas token for your azure iot device
  * should look like this: SharedAccessSignature sr=AZURE_HUB_NAME.azure-devices.net...
* AD009_USER = username of AD009 to connect to this bridge
* AD009_PASSWORD = password of AD009 to connect to this bridge
* IGNORE_BROKER_TLS_VALIDATION set to true, to disable TLS valition to the azure broker