#!/bin/ash

envsubst < /mosquitto/config/mosquitto.conf.template > /mosquitto/config/mosquitto.conf
envsubst < /mosquitto/config/passwords.template > /mosquitto/config/passwords
chmod 0700 /mosquitto/config/passwords.template
chmod 0700 /mosquitto/config/passwords
mosquitto_passwd -U /mosquitto/config/passwords
set -e

# Set permissions
user="$(id -u)"
if [ "$user" = '0' ]; then
        [ -d "/mosquitto" ] && chown -R mosquitto:mosquitto /mosquitto || true
fi
exec /usr/sbin/mosquitto -c /mosquitto/config/mosquitto.conf